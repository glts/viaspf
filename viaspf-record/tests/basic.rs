use std::{net::Ipv6Addr, num::NonZeroU8};
use viaspf_record::*;

#[test]
fn monster_record() {
    let record = SpfRecord {
        terms: vec![
            Term::Directive(Directive {
                qualifier: Some(Qualifier::Pass),
                mechanism: Mechanism::Mx(Mx {
                    domain_spec: None,
                    prefix_len: None,
                }),
            }),
            Term::Directive(Directive {
                qualifier: None,
                mechanism: Mechanism::A(A {
                    domain_spec: Some(
                        DomainSpec::new(MacroString {
                            segments: vec![MacroStringSegment::MacroLiteral(
                                MacroLiteral::new("example.org").unwrap(),
                            )],
                        })
                        .unwrap(),
                    ),
                    prefix_len: None,
                }),
            }),
            Term::Directive(Directive {
                qualifier: Some(Qualifier::Pass),
                mechanism: Mechanism::A(A {
                    domain_spec: None,
                    prefix_len: Some(DualCidrLength::Both(
                        Ip4CidrLength::new(21).unwrap(),
                        Ip6CidrLength::new(56).unwrap(),
                    )),
                }),
            }),
            Term::Directive(Directive {
                qualifier: None,
                mechanism: Mechanism::Ip6(Ip6 {
                    addr: Ipv6Addr::new(0x4008, 0x2c, 0, 0, 0, 0x1, 0x2, 0x3),
                    prefix_len: Some(Ip6CidrLength::new(104).unwrap()),
                }),
            }),
            Term::Directive(Directive {
                qualifier: Some(Qualifier::Softfail),
                mechanism: Mechanism::Include(Include {
                    domain_spec: DomainSpec::new(MacroString {
                        segments: vec![
                            MacroStringSegment::MacroExpand(MacroExpand::Escape(Escape::Percent)),
                            MacroStringSegment::MacroExpand(MacroExpand::Macro({
                                let mut m = Macro::new(MacroKind::LocalPart);
                                m.set_limit(NonZeroU8::new(1).unwrap());
                                m.set_reverse(true);
                                m.set_delimiters(Delimiters::new(".+-,").unwrap());
                                m
                            })),
                            MacroStringSegment::MacroLiteral(
                                MacroLiteral::new("._local.example.org").unwrap(),
                            ),
                        ],
                    })
                    .unwrap(),
                }),
            }),
            Term::Modifier(Modifier::Explanation(Explanation {
                domain_spec: DomainSpec::new(MacroString {
                    segments: vec![
                        MacroStringSegment::MacroExpand(MacroExpand::Macro(Macro::new(
                            MacroKind::HeloDomain,
                        ))),
                        MacroStringSegment::MacroLiteral(
                            MacroLiteral::new(".example.org").unwrap(),
                        ),
                    ],
                })
                .unwrap(),
            })),
            Term::Modifier(Modifier::Unknown(
                Unknown::new(
                    Name::new("x-extension").unwrap(),
                    MacroString {
                        segments: vec![
                            MacroStringSegment::MacroLiteral(MacroLiteral::new("example").unwrap()),
                            MacroStringSegment::MacroExpand(MacroExpand::Escape(Escape::Space)),
                            MacroStringSegment::MacroLiteral(MacroLiteral::new("ext").unwrap()),
                        ],
                    },
                )
                .unwrap(),
            )),
        ],
    };

    let record_str = "v=spf1 \
                      +mx \
                      a:example.org \
                      +a/21//56 \
                      ip6:4008:2c::1:2:3/104 \
                      ~include:%%%{l1r.+-,}._local.example.org \
                      exp=%{h}.example.org \
                      x-extension=example%_ext";

    assert_eq!(record.to_string(), record_str);
    assert_eq!(record_str.parse(), Ok(record));
}
