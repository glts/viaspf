# viaspf-record changelog

All notable, publicly visible changes are listed in this changelog.

This project follows Rust-flavoured [semantic versioning], with changes to the
minimum supported Rust version being considered breaking changes.

[semantic versioning]: https://doc.rust-lang.org/cargo/reference/semver.html

## 0.5.1 (2024-05-06)

This is a maintenance release with no notable changes. With this release,
development has moved from GitLab to the [Codeberg] platform.

[Codeberg]: https://codeberg.org

## 0.5.0 (2023-09-12)

The minimum supported Rust version is now 1.65.0.

### Changed

* The minimum supported Rust version has been raised to 1.65.0.
* The main struct `Record` has been renamed to `SpfRecord`.
* Data types wrapping `String` have been changed to wrap `Box<str>`.

## 0.4.1 (2022-05-10)

### Added

* `MacroString` can now be parsed from a string via `std::str::FromStr`.

## 0.4.0 (2022-03-06)

Initial release.

This package was split out from the [viaspf] package, where it used to live as
module `viaspf::record`. The initial version is 0.4.0, the version when the
split took place. From now on, this package and its version number will be
updated independently from viaspf.

### Changed

* Parsing errors are now encoded with the new [`ParseError`] struct.

[viaspf]: https://crates.io/crates/viaspf
[`ParseError`]: https://docs.rs/viaspf-record/0.4.0/viaspf_record/struct.ParseError.html
