// viaspf – implementation of the SPF specification
// Copyright © 2020–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

pub trait StripPrefixIgnoreCase {
    fn strip_prefix_ignore_case(&self, prefix: &str) -> Option<&Self>;
}

impl StripPrefixIgnoreCase for str {
    fn strip_prefix_ignore_case(&self, prefix: &str) -> Option<&Self> {
        if matches!(self.get(..prefix.len()), Some(s) if s.eq_ignore_ascii_case(prefix)) {
            Some(&self[prefix.len()..])
        } else {
            None
        }
    }
}

pub trait WithoutSuffix {
    fn without_suffix(&self, suffix: &str) -> &Self;
}

impl WithoutSuffix for str {
    fn without_suffix(&self, suffix: &str) -> &Self {
        debug_assert!(self.ends_with(suffix));
        self.len().checked_sub(suffix.len())
            .and_then(|i| self.get(..i))
            .unwrap_or(self)
    }
}

pub trait StripChar {
    fn strip_char(&self, c: &[char]) -> Option<(&Self, char)>;
}

impl StripChar for str {
    fn strip_char(&self, c: &[char]) -> Option<(&Self, char)> {
        self.strip_prefix(c)
            .map(|i| (i, self.chars().next().unwrap()))
    }
}

pub trait ScanMany {
    fn scan_many<F>(&self, f: F) -> Option<&Self>
    where
        F: for<'a> Fn(&'a Self) -> Option<&'a Self>;

    fn scan_many_n<F>(&self, max: usize, f: F) -> Option<&Self>
    where
        F: for<'a> Fn(&'a Self) -> Option<&'a Self>;
}

impl ScanMany for str {
    fn scan_many<F>(&self, f: F) -> Option<&Self>
    where
        F: for<'a> Fn(&'a Self) -> Option<&'a Self>,
    {
        let mut i = f(self)?;
        while let Some(ix) = f(i) {
            i = ix;
        }
        Some(i)
    }

    fn scan_many_n<F>(&self, max: usize, f: F) -> Option<&Self>
    where
        F: for<'a> Fn(&'a Self) -> Option<&'a Self>,
    {
        assert!(max > 0);
        let mut i = f(self)?;
        for _ in 0..(max - 1) {
            if let Some(ix) = f(i) {
                i = ix;
            } else {
                break;
            }
        }
        Some(i)
    }
}

pub trait CollectMany<T> {
    fn collect_many<F>(&self, f: F) -> Option<(&Self, Vec<T>)>
    where
        F: for<'a> Fn(&'a Self) -> Option<(&'a Self, T)>;

    fn collect_many_n<F>(&self, max: usize, f: F) -> Option<(&Self, Vec<T>)>
    where
        F: for<'a> Fn(&'a Self) -> Option<(&'a Self, T)>;
}

impl<T> CollectMany<T> for str {
    fn collect_many<F>(&self, f: F) -> Option<(&Self, Vec<T>)>
    where
        F: for<'a> Fn(&'a Self) -> Option<(&'a Self, T)>,
    {
        let (mut i, x) = f(self)?;
        let mut xs = vec![x];
        while let Some((ix, x)) = f(i) {
            i = ix;
            xs.push(x);
        }
        Some((i, xs))
    }

    fn collect_many_n<F>(&self, max: usize, f: F) -> Option<(&Self, Vec<T>)>
    where
        F: for<'a> Fn(&'a Self) -> Option<(&'a Self, T)>,
    {
        assert!(max > 0);
        let (mut i, x) = f(self)?;
        let mut xs = vec![x];
        for _ in 0..(max - 1) {
            if let Some((ix, x)) = f(i) {
                i = ix;
                xs.push(x);
            } else {
                break;
            }
        }
        Some((i, xs))
    }
}

pub trait MapResult<'a, T> {
    fn map_result<U, F: FnOnce(T) -> U>(self, f: F) -> Option<(&'a str, U)>;
}

impl<'a, T> MapResult<'a, T> for Option<(&'a str, T)> {
    fn map_result<U, F: FnOnce(T) -> U>(self, f: F) -> Option<(&'a str, U)> {
        self.map(|(i, x)| (i, f(x)))
    }
}

pub trait FilterResult<'a, T> {
    fn filter_result<F: FnOnce(&T) -> bool>(self, f: F) -> Option<(&'a str, T)>;
}

impl<'a, T> FilterResult<'a, T> for Option<(&'a str, T)> {
    fn filter_result<F: FnOnce(&T) -> bool>(self, f: F) -> Self {
        self.filter(|(_, x)| f(x))
    }
}

pub trait UnwrapOrNoneResult<'a, T> {
    fn unwrap_or_none_result(self, input: &'a str) -> (&'a str, Option<T>);
}

impl<'a, T> UnwrapOrNoneResult<'a, T> for Option<(&'a str, T)> {
    fn unwrap_or_none_result(self, input: &'a str) -> (&'a str, Option<T>) {
        self.map_result(Some).unwrap_or((input, None))
    }
}

pub trait UnwrapOrDefaultResult<'a, T: Default> {
    fn unwrap_or_default_result(self, input: &'a str) -> (&'a str, T);
}

impl<'a, T: Default> UnwrapOrDefaultResult<'a, T> for Option<(&'a str, T)> {
    fn unwrap_or_default_result(self, input: &'a str) -> (&'a str, T) {
        self.unwrap_or_else(|| (input, Default::default()))
    }
}
