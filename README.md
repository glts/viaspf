# viaspf

The **viaspf** project is a complete implementation of the *Sender Policy
Framework* (SPF) specification, version 1, as described in [RFC 7208]. It is
written in the Rust programming language.

This project is organised in two packages:

* **[viaspf]** contains a complete implementation of the SPF specification. It
  provides an asynchronous API that allows checking an email sender’s
  authorisation according to the specification.
* **[viaspf-record]** contains a data model and parser for SPF records.

See the documentation of these packages for details.

[RFC 7208]: https://www.rfc-editor.org/rfc/rfc7208
[viaspf]: https://codeberg.org/glts/viaspf/src/branch/main/viaspf
[viaspf-record]: https://codeberg.org/glts/viaspf/src/branch/main/viaspf-record

## Licence

Copyright © 2020–2024 David Bürgin

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
