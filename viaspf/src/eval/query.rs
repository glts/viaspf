// viaspf – implementation of the SPF specification
// Copyright © 2020–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{
    config::Config,
    eval::{EvalError, EvalResult, Evaluate},
    lookup::{Lookup, LookupError, LookupResult, Name},
    params::Sender,
    record::SpfRecord,
    result::{ErrorCause, SpfResult, SpfResultCause},
    trace::{Trace, Tracepoint},
};
use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    mem,
    net::{IpAddr, Ipv4Addr, Ipv6Addr},
};

pub struct Resolver<'l> {
    lookup: &'l dyn Lookup,
}

impl<'l> Resolver<'l> {
    pub fn new(lookup: &'l impl Lookup) -> Self {
        Self { lookup }
    }

    pub async fn lookup_a(
        &self,
        query: &mut Query<'_>,
        name: &Name,
    ) -> LookupResult<Vec<Ipv4Addr>> {
        trace!(query, Tracepoint::LookupA(name.clone()));
        self.lookup.lookup_a(name).await
    }

    pub async fn lookup_aaaa(
        &self,
        query: &mut Query<'_>,
        name: &Name,
    ) -> LookupResult<Vec<Ipv6Addr>> {
        trace!(query, Tracepoint::LookupAaaa(name.clone()));
        self.lookup.lookup_aaaa(name).await
    }

    pub async fn lookup_a_or_aaaa(
        &self,
        query: &mut Query<'_>,
        name: &Name,
        ip: IpAddr,
    ) -> LookupResult<Vec<IpAddr>> {
        // §5: ‘When any mechanism fetches host addresses to compare with <ip>,
        // when <ip> is an IPv4, "A" records are fetched; when <ip> is an IPv6
        // address, "AAAA" records are fetched.’
        match ip {
            IpAddr::V4(_) => self
                .lookup_a(query, name)
                .await
                .map(|addrs| addrs.into_iter().map(From::from).collect()),
            IpAddr::V6(_) => self
                .lookup_aaaa(query, name)
                .await
                .map(|addrs| addrs.into_iter().map(From::from).collect()),
        }
    }

    pub async fn lookup_mx(&self, query: &mut Query<'_>, name: &Name) -> LookupResult<Vec<Name>> {
        trace!(query, Tracepoint::LookupMx(name.clone()));
        self.lookup.lookup_mx(name).await
    }

    pub async fn lookup_txt(
        &self,
        query: &mut Query<'_>,
        name: &Name,
    ) -> LookupResult<Vec<String>> {
        trace!(query, Tracepoint::LookupTxt(name.clone()));
        self.lookup.lookup_txt(name).await
    }

    pub async fn lookup_ptr(&self, query: &mut Query<'_>, ip: IpAddr) -> LookupResult<Vec<Name>> {
        trace!(query, Tracepoint::LookupPtr(ip));
        self.lookup.lookup_ptr(ip).await
    }
}

pub struct Query<'c> {
    pub config: &'c Config,
    pub params: QueryParams,
    pub state: QueryState,
    pub result_cause: Option<SpfResultCause>,
    pub trace: Trace,
}

impl<'c> Query<'c> {
    pub fn new(config: &'c Config, params: QueryParams, state: QueryState) -> Self {
        Self {
            config,
            params,
            state,
            result_cause: Default::default(),
            trace: Default::default(),
        }
    }

    pub async fn execute(&mut self, resolver: &Resolver<'_>) -> SpfResult {
        trace!(self, Tracepoint::ExecuteQuery(self.params.domain.clone()));

        let spf_result = self.execute_internal(resolver).await;

        trace!(self, Tracepoint::QueryResult(spf_result.clone()));

        spf_result
    }

    async fn execute_internal(&mut self, resolver: &Resolver<'_>) -> SpfResult {
        // This use of `clone` only to satisfy the borrow checker.
        let domain = self.params.domain.clone();

        let spf_record = match lookup_spf_record(resolver, self, &domain).await {
            Ok(r) => r,

            // §4.4: ‘If the DNS lookup returns a server failure (RCODE 2) or
            // some other error (RCODE other than 0 or 3), or if the lookup
            // times out, then check_host() terminates immediately with the
            // result "temperror".’
            Err(SpfRecordLookupError::DnsLookup(e)) => {
                trace!(self, Tracepoint::SpfRecordLookupError(e));
                self.result_cause = Some(ErrorCause::Dns.into());
                return SpfResult::Temperror;
            }

            // §4.5: ‘If the resultant record set includes no records,
            // check_host() produces the "none" result.’
            Err(SpfRecordLookupError::NoRecord) => {
                trace!(self, Tracepoint::NoSpfRecord);
                return SpfResult::None;
            }

            // §4.5: ‘If the resultant record set includes more than one record,
            // check_host() produces the "permerror" result.’
            Err(SpfRecordLookupError::MultipleRecords(records)) => {
                trace!(self, Tracepoint::MultipleSpfRecords(records));
                self.result_cause = Some(ErrorCause::MultipleSpfRecords.into());
                return SpfResult::Permerror;
            }

            // §4.6: ‘if there are any syntax errors anywhere in the record,
            // check_host() returns immediately with the result "permerror",
            // without further interpretation or evaluation.’
            Err(SpfRecordLookupError::Syntax(s)) => {
                trace!(self, Tracepoint::InvalidSpfRecordSyntax(s));
                self.result_cause = Some(ErrorCause::InvalidSpfRecordSyntax.into());
                return SpfResult::Permerror;
            }
        };

        spf_record.evaluate(self, resolver).await
    }
}

#[derive(Debug)]
enum SpfRecordLookupError {
    DnsLookup(LookupError),
    NoRecord,
    MultipleRecords(Vec<String>),
    Syntax(String),
}

impl Error for SpfRecordLookupError {}

impl Display for SpfRecordLookupError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "failed to obtain SPF record")
    }
}

impl From<LookupError> for SpfRecordLookupError {
    fn from(error: LookupError) -> Self {
        match error {
            LookupError::NoRecords => Self::NoRecord,
            _ => Self::DnsLookup(error),
        }
    }
}

async fn lookup_spf_record(
    resolver: &Resolver<'_>,
    query: &mut Query<'_>,
    name: &Name,
) -> Result<SpfRecord, SpfRecordLookupError> {
    let mut records = resolver
        .lookup_txt(query, name)
        .await?
        .into_iter()
        .filter(|s| is_spf_record(s));

    use SpfRecordLookupError::*;
    match records.next() {
        None => Err(NoRecord),
        Some(record) => {
            let mut rest = records.collect::<Vec<_>>();
            match *rest {
                [] => record.parse().map_err(|_| Syntax(record)),
                [..] => {
                    rest.insert(0, record);
                    Err(MultipleRecords(rest))
                }
            }
        }
    }
}

// §4.5: ‘Starting with the set of records that were returned by the lookup,
// discard records that do not begin with a version section of exactly
// "v=spf1".’
fn is_spf_record(s: &str) -> bool {
    let prefix = "v=spf1";
    matches!(s.get(..prefix.len()), Some(s) if s.eq_ignore_ascii_case(prefix))
        && (s.len() == prefix.len() || s[prefix.len()..].starts_with(' '))
}

pub struct QueryParams {
    ip: IpAddr,
    domain: Name,
    sender: Sender,
    helo_domain: String,
}

impl QueryParams {
    pub fn new(ip: IpAddr, domain: Name, sender: Sender, helo_domain: String) -> Self {
        Self {
            ip,
            domain,
            sender,
            helo_domain,
        }
    }

    pub fn ip(&self) -> IpAddr {
        self.ip
    }

    pub fn domain(&self) -> &Name {
        &self.domain
    }

    pub fn sender(&self) -> &Sender {
        &self.sender
    }

    pub fn helo_domain(&self) -> &str {
        &self.helo_domain
    }

    pub fn replace_domain(&mut self, domain: Name) -> Name {
        mem::replace(&mut self.domain, domain)
    }
}

#[derive(Default)]
pub struct QueryState {
    lookup_count: usize,
    void_lookup_count: usize,
    included_query: bool,
    eval_explain_string: bool,
}

impl QueryState {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn increment_lookup_count(&mut self, max: usize) -> EvalResult<()> {
        if self.lookup_count < max {
            self.lookup_count += 1;
            Ok(())
        } else {
            Err(EvalError::LookupLimitExceeded)
        }
    }

    pub fn increment_void_lookup_count(&mut self, max: usize) -> EvalResult<()> {
        if self.void_lookup_count < max {
            self.void_lookup_count += 1;
            Ok(())
        } else {
            Err(EvalError::VoidLookupLimitExceeded)
        }
    }

    pub fn is_included_query(&self) -> bool {
        self.included_query
    }

    pub fn set_included_query(&mut self, included: bool) {
        self.included_query = included;
    }

    pub fn is_eval_explain_string(&self) -> bool {
        self.eval_explain_string
    }

    pub fn reset_for_explain_string(&mut self) {
        self.lookup_count = Default::default();
        self.void_lookup_count = Default::default();
        self.eval_explain_string = true;
    }
}
