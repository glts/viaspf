// viaspf – implementation of the SPF specification
// Copyright © 2020–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{
    eval::{
        query::{Query, Resolver},
        terms, EvalError, EvalResult, EvaluateToString,
    },
    record::{
        Delimiters, DomainSpec, Escape, ExplainString, ExplainStringSegment, Macro, MacroExpand,
        MacroKind, MacroLiteral, MacroString, MacroStringSegment,
    },
    trace::Tracepoint,
};
use async_trait::async_trait;
use std::{
    fmt::Write,
    net::IpAddr,
    num::NonZeroU8,
    str,
    time::{Duration, SystemTime},
};

// Note that failure when evaluating macro `p` is only a theoretical
// possibility. Evaluation of `p` can only fail due to lookup limits (global or
// void lookup limit), so only a crafted explain string like `"%{p}%{p}%{p}..."`
// can fail. Nevertheless, the explain string is sanitised by replacing all uses
// of `%{p}` with the default value `unknown`.
pub async fn safely_expand_explain_string(
    query: &mut Query<'_>,
    resolver: &Resolver<'_>,
    explain_string: &ExplainString,
) -> String {
    explain_string
        .clone()
        .into_iter()
        .map(|segment| match segment {
            ExplainStringSegment::MacroString(macro_string) => {
                ExplainStringSegment::MacroString(sanitize_macro_string(macro_string))
            }
            segment => segment,
        })
        .collect::<ExplainString>()
        .evaluate_to_string(query, resolver)
        .await
        .expect("failed to evaluate sanitized explain string")
}

fn sanitize_macro_string(macro_string: MacroString) -> MacroString {
    macro_string
        .into_iter()
        .map(|segment| match segment {
            MacroStringSegment::MacroExpand(MacroExpand::Macro(m))
                if m.kind() == MacroKind::ValidatedDomain =>
            {
                MacroLiteral::new(default_validated_domain_string())
                    .expect("invalid default validated domain")
                    .into()
            }
            segment => segment,
        })
        .collect()
}

#[async_trait]
impl EvaluateToString for DomainSpec {
    async fn evaluate_to_string(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<String> {
        trace!(query, Tracepoint::ExpandDomainSpec(self.clone()));

        self.as_ref().evaluate_to_string(query, resolver).await
    }
}

#[async_trait]
impl EvaluateToString for ExplainString {
    async fn evaluate_to_string(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<String> {
        trace!(query, Tracepoint::ExpandExplainString(self.clone()));

        let mut result = String::new();
        for segment in &self.segments {
            match segment {
                ExplainStringSegment::MacroString(macro_string) => {
                    result += &macro_string.evaluate_to_string(query, resolver).await?;
                }
                space @ ExplainStringSegment::Space => {
                    result += &space.to_string();
                }
            }
        }
        Ok(result)
    }
}

#[async_trait]
impl EvaluateToString for MacroString {
    async fn evaluate_to_string(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<String> {
        let mut result = String::new();
        for segment in &self.segments {
            match segment {
                MacroStringSegment::MacroExpand(macro_expand) => match macro_expand {
                    MacroExpand::Macro(macro_) => {
                        result += &macro_.evaluate_to_string(query, resolver).await?;
                    }
                    MacroExpand::Escape(escape) => {
                        result += match escape {
                            Escape::Percent => "%",
                            Escape::Space => " ",
                            Escape::UrlEncodedSpace => "%20",
                        };
                    }
                },
                MacroStringSegment::MacroLiteral(macro_literal) => {
                    result += macro_literal.as_ref();
                }
            }
        }
        Ok(result)
    }
}

#[async_trait]
impl EvaluateToString for Macro {
    async fn evaluate_to_string(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<String> {
        // §7.2: ‘The following macro letters are allowed only in "exp" text:’,
        // namely ‘c’, ‘r’, and ‘t’.
        use MacroKind::*;
        if !query.state.is_eval_explain_string()
            && matches!(self.kind(), PrettyIp | Receiver | Timestamp)
        {
            return Err(EvalError::InvalidMacroInDomainSpec(self.clone()));
        }
        let mut s = match self.kind() {
            Sender => query.params.sender().to_string(),
            LocalPart => query.params.sender().local_part().to_owned(),
            DomainPart => query.params.sender().domain().to_string(),
            Domain => query.params.domain().to_string(),
            Ip => to_dot_format_string(query.params.ip()),
            ValidatedDomain => validated_domain_string(query, resolver).await?,
            HeloDomain => query.params.helo_domain().to_owned(),
            PrettyIp => query.params.ip().to_string(),
            Receiver => to_hostname_string(query.config.hostname()),
            Timestamp => timestamp_in_secs(),
            VersionLabel => to_version_label_string(query.params.ip()),
        };

        // §7.3: ‘If transformers or delimiters are provided,’ …
        if self.limit().is_some() || self.reverse() || self.delimiters().is_some() {
            // … ‘the replacement value for a macro letter is split into parts
            // separated by one or more of the specified delimiter characters.’
            let mut parts = match self.delimiters() {
                None => split_on_delimiters(&s, &Default::default()),
                Some(delimiters) => split_on_delimiters(&s, delimiters),
            };

            // ‘After performing any reversal operation and/or removal of
            // left-hand parts, the parts are rejoined using "." and not the
            // original splitting characters.’
            if self.reverse() {
                parts.reverse();
            }
            if let Some(limit) = self.limit() {
                truncate_parts(&mut parts, limit);
            }
            s = parts.join(".");
        }

        // ‘Uppercase macros expand exactly as their lowercase equivalents, and
        // are then URL escaped.’
        if self.url_encode() {
            s = url_encode(&s);
        }

        Ok(s)
    }
}

fn to_dot_format_string(addr: IpAddr) -> String {
    match addr {
        IpAddr::V4(addr) => addr.to_string(),
        IpAddr::V6(addr) => addr
            .octets()
            .iter()
            .map(|o| format!("{:x}.{:x}", o >> 4, o & 0xf))
            .collect::<Vec<_>>()
            .join("."),
    }
}

async fn validated_domain_string(
    query: &mut Query<'_>,
    resolver: &Resolver<'_>,
) -> EvalResult<String> {
    let ip = query.params.ip();

    let ptrs = match terms::to_eval_result(resolver.lookup_ptr(query, ip).await) {
        Ok(ptrs) => ptrs,
        // §7.3: ‘if a DNS error occurs, the string "unknown" is used.’
        Err(e) => {
            trace!(query, Tracepoint::ReverseLookupError(e));
            return Ok(default_validated_domain_string());
        }
    };
    terms::increment_void_lookup_count_if_void(query, ptrs.len())?;

    // The only way this function (thus, the `p` macro) may fail is by stepping
    // over a lookup limit, either just above or in the following call:
    let validated_names = terms::get_validated_domain_names(query, resolver, ip, ptrs).await?;

    let domain = query.params.domain();

    // §7.3: ‘If the <domain> is present in the list of validated domains, it
    // SHOULD be used.’
    for name in &validated_names {
        trace!(query, Tracepoint::TryValidatedNameExactMatch(name.clone()));
        if name == domain {
            return Ok(name.to_string());
        }
    }

    // ‘Otherwise, if a subdomain of the <domain> is present, it SHOULD be
    // used.’
    for name in &validated_names {
        trace!(query, Tracepoint::TryValidatedNameSubdomainMatch(name.clone()));
        if name.is_subdomain_of(domain) {
            return Ok(name.to_string());
        }
    }

    // ‘Otherwise, any name from the list can be used. If there are no validated
    // domain names […], the string "unknown" is used.’
    Ok(match validated_names.first() {
        None => {
            trace!(query, Tracepoint::DefaultUnknownValidatedName);
            default_validated_domain_string()
        }
        Some(name) => name.to_string(),
    })
}

pub fn default_validated_domain_string() -> String {
    "unknown".into()
}

// §7.3: ‘This SHOULD be a fully qualified domain name, but if one does not
// exist […] or if policy restrictions dictate otherwise, the word "unknown"
// SHOULD be substituted.’
fn to_hostname_string(hostname: Option<&str>) -> String {
    hostname.map_or_else(|| "unknown".into(), |s| s.to_owned())
}

fn timestamp_in_secs() -> String {
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap_or(Duration::ZERO)
        .as_secs()
        .to_string()
}

fn to_version_label_string(addr: IpAddr) -> String {
    match addr {
        IpAddr::V4(_) => "in-addr",
        IpAddr::V6(_) => "ip6",
    }
    .into()
}

fn url_encode(s: &str) -> String {
    let mut result = String::with_capacity(s.len());
    for b in s.bytes() {
        // §7.3: ‘URL escaping MUST be performed for characters not in the
        // "unreserved" set,’ see RFC 3986, §2.3.
        if is_unreserved(b) {
            result.push(b.into());
        } else {
            write!(result, "%{b:02X}").unwrap();
        }
    }
    result
}

fn is_unreserved(b: u8) -> bool {
    b.is_ascii_alphanumeric() || matches!(b, b'-' | b'.' | b'_' | b'~')
}

fn split_on_delimiters<'a>(s: &'a str, delimiters: &Delimiters) -> Vec<&'a str> {
    // Since delimiters are ASCII-only, working on bytes and turning byte slices
    // back into strings is correct.
    let s = s.as_bytes();
    let delimiters = delimiters.as_ref().as_bytes();

    let mut parts = Vec::new();
    let mut i = 0;
    let mut start = i;

    while i < s.len() {
        if delimiters.contains(&s[i]) {
            parts.push(str::from_utf8(&s[start..i]).unwrap());
            i += 1;
            while i < s.len() && delimiters.contains(&s[i]) {
                i += 1;
            }
            start = i;
        } else {
            i += 1;
        }
    }

    parts.push(str::from_utf8(&s[start..i]).unwrap());

    parts
}

fn truncate_parts<T>(parts: &mut Vec<T>, limit: NonZeroU8) {
    let i = parts.len().saturating_sub(limit.get().into());
    parts.drain(..i);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn split_on_delimiters_ok() {
        fn split<'a>(s: &'a str, delimiters: &str) -> Vec<&'a str> {
            split_on_delimiters(s, &Delimiters::new(delimiters).unwrap())
        }

        assert_eq!(split("abc", "."), ["abc"]);
        assert_eq!(split("", "."), [""]);
        assert_eq!(split("abc.", "."), ["abc", ""]);
        assert_eq!(split(".abc", "."), ["", "abc"]);
        assert_eq!(split(".", "."), ["", ""]);

        assert_eq!(split("a,.bc.d", ".,+"), ["a", "bc", "d"]);
        assert_eq!(split("+.a,+.", ".,+"), ["", "a", ""]);
        assert_eq!(split(",++.,", ".,+"), ["", ""]);
        assert_eq!(split("++.,ab", ".,+"), ["", "ab"]);
        assert_eq!(split("c,..", ".,+"), ["c", ""]);
    }

    #[test]
    fn timestamp_in_secs_ok() {
        let t = timestamp_in_secs();
        assert_eq!(t.len(), 10);
        assert!(t.chars().all(|c| c.is_ascii_digit()));
    }

    #[test]
    fn url_escape_ok() {
        assert_eq!(&url_encode("me@gluet.ch"), "me%40gluet.ch");
        assert_eq!(&url_encode("我@gluet.ch"), "%E6%88%91%40gluet.ch");
    }

    #[test]
    fn sanitize_macro_string_ok() {
        let ms = "%{d}x%{P}".parse::<MacroString>().unwrap();

        assert_eq!(sanitize_macro_string(ms).to_string(), "%{d}xunknown");
    }
}
