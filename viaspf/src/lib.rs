// viaspf – implementation of the SPF specification
// Copyright © 2020–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

//! A library implementing the *Sender Policy Framework* (SPF) specification,
//! version 1, described in [RFC 7208].
//!
//! This library provides an API for checking whether an email sending host is
//! authorised to use some mail domain according to SPF policy published in the
//! DNS.
//!
//! # Usage
//!
//! The function [`evaluate_sender`] is the main public API item. It allows
//! checking the MAIL FROM and HELO identities according to the specification.
//!
//! In addition, the function [`expand_explain_string`] allows performing macro
//! expansion on *explain strings* (macro strings).
//!
//! The above functions both require to be passed a `Lookup` implementation,
//! which serves as a DNS resolver. The [`Lookup`][crate::lookup::Lookup] trait
//! allows you to plug in any DNS resolver yourself, by providing an appropriate
//! implementation. See the module [`lookup`] for more.
//!
//! As a convenience, a `Lookup` implementation for the [Hickory DNS] resolver
//! can be made available by enabling the Cargo feature `hickory-resolver`.
//!
//! # Cargo features
//!
//! The feature **`hickory-resolver`** can be enabled to make a `Lookup`
//! implementation available for the asynchronous [Tokio]-based
//! [`hickory_resolver::TokioAsyncResolver`].
//!
//! The default feature **`tokio-timeout`** enables timeout logic using the
//! [Tokio] `time` module. With this feature, the query functions will abort
//! after the configured timeout. If you want to use a different async runtime,
//! you can disable this feature and implement timeouts yourself (for example,
//! as part of the `Lookup` implementation). Thus you can use this library
//! without depending on Tokio at all.
//!
//! [RFC 7208]: https://www.rfc-editor.org/rfc/rfc7208
//! [*check_host()*]: https://www.rfc-editor.org/rfc/rfc7208#section-4
//! [Hickory DNS]: https://github.com/hickory-dns/hickory-dns
//! [`hickory_resolver::TokioAsyncResolver`]: https://docs.rs/hickory-resolver/0.24.0/hickory_resolver/type.TokioAsyncResolver.html
//! [Tokio]: https://tokio.rs

macro_rules! trace {
    ($query:ident, $tracepoint:expr) => {
        if $query.config.capture_trace() {
            $query.trace.add_event($tracepoint);
        }
    };
}

mod config;
mod eval;
pub mod lookup;
mod params;
mod result;
pub mod trace;

pub use viaspf_record as record;

pub use crate::{
    config::{Config, ConfigBuilder},
    eval::{evaluate_sender, expand_explain_string, EvalError},
    params::{DomainName, ParseParamError, Sender},
    result::{
        ErrorCause, ExpansionResult, ExplanationString, QueryResult, SpfResult, SpfResultCause,
    },
};

// The implementation includes quotations (marked with ‘§’) from RFC 7208, to
// explain inline certain implementation choices. The RFC document can be found
// at https://www.rfc-editor.org/rfc/rfc7208.
