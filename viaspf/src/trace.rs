// viaspf – implementation of the SPF specification
// Copyright © 2020–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

//! Tracing information gathered during SPF query execution.

use crate::{
    eval::EvalError,
    lookup::{LookupError, Name},
    record::{Directive, DomainSpec, ExplainString, Explanation, Mechanism, Redirect, SpfRecord},
    result::{ExplanationString, SpfResult},
};
use std::{
    fmt::{self, Display, Formatter},
    iter::FromIterator,
    net::IpAddr,
    time::SystemTime,
    vec::IntoIter,
};

/// A trace of processing events.
#[derive(Debug, Default)]
pub struct Trace {
    /// The events recorded in this trace.
    pub events: Vec<TraceEvent>,
}

impl Trace {
    /// Creates a new trace.
    pub fn new() -> Self {
        Default::default()
    }

    /// Adds a tracepoint to the trace.
    pub fn add_event(&mut self, tracepoint: Tracepoint) {
        self.events.push(TraceEvent {
            timestamp: SystemTime::now(),
            tracepoint,
        });
    }
}

impl IntoIterator for Trace {
    type Item = TraceEvent;
    type IntoIter = IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.events.into_iter()
    }
}

impl From<Vec<TraceEvent>> for Trace {
    fn from(events: Vec<TraceEvent>) -> Self {
        Self { events }
    }
}

impl FromIterator<TraceEvent> for Trace {
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = TraceEvent>,
    {
        Self {
            events: iter.into_iter().collect(),
        }
    }
}

/// A trace event that occurred during processing of a query.
#[derive(Debug)]
pub struct TraceEvent {
    /// The event’s timestamp.
    pub timestamp: SystemTime,
    /// The event’s tracepoint.
    pub tracepoint: Tracepoint,
}

/// A milestone passed through during processing of a query.
#[derive(Debug)]
#[non_exhaustive]
pub enum Tracepoint {
    // Note that tracepoints do not necessarily point to a unique location, some
    // are used multiple times.

    // Query
    /// An SPF query for a domain is being executed.
    ExecuteQuery(Name),
    /// An SPF query was evaluated to a result.
    QueryResult(SpfResult),

    // SPF record lookup
    /// An error occurred while querying for SPF records.
    SpfRecordLookupError(LookupError),
    /// No SPF record was found.
    NoSpfRecord,
    /// Multiple SPF records were found.
    MultipleSpfRecords(Vec<String>),
    /// An SPF record could not be parsed.
    InvalidSpfRecordSyntax(String),

    // Evaluate SPF record
    /// An SPF record is being evaluated.
    EvaluateSpfRecord(SpfRecord),
    /// Multiple *redirect* modifiers were found in an SPF record.
    MultipleRedirectModifiers(Vec<Redirect>),
    /// Multiple *exp* modifiers were found in an SPF record.
    MultipleExpModifiers(Vec<Explanation>),
    /// A directive is being evaluated.
    EvaluateDirective(Directive),
    /// A mechanism is being evaluated.
    EvaluateMechanism(Mechanism),
    /// A mechanism matched.
    MechanismMatch,
    /// A mechanism did not match.
    MechanismNoMatch,
    /// A mechanism was evaluated to an SPF result after evaluation failed with
    /// an error.
    MechanismErrorResult(EvalError, SpfResult),
    /// A directive was evaluated to an SPF result.
    DirectiveResult(SpfResult),
    /// A *redirect* modifier was evaluated to an SPF result.
    RedirectResult(SpfResult),
    /// The default result *neutral* is being returned.
    NeutralResult,

    // Terms
    /// An IP address is being checked for a match.
    TryIpAddr(IpAddr),
    /// An MX name is being checked for a match.
    TryMxName(Name),
    /// A PTR name is being validated.
    ValidatePtrName(Name),
    /// An error occurred while doing a reverse lookup.
    ReverseLookupError(EvalError),
    /// The address lookup limit was exceeded while validating PTR names.
    PtrAddressLookupLimitExceeded,
    /// An error occurred while querying PTR addresses.
    PtrAddressLookupError(EvalError),
    /// A PTR name was validated.
    PtrNameValidated,
    /// A validated domain name is being checked for a match.
    TryValidatedName(Name),
    /// A validated domain name is being checked for an exact match.
    TryValidatedNameExactMatch(Name),
    /// A validated domain name is being checked for a subdomain match.
    TryValidatedNameSubdomainMatch(Name),
    /// The default unknown validated domain name is being returned.
    DefaultUnknownValidatedName,
    /// A *redirect* modifier is being evaluated.
    EvaluateRedirect(Redirect),
    /// No SPF record was found at the target domain for a *redirect* modifier.
    RedirectNoSpfRecord,
    /// The global lookup limit was exceeded while evaluating a *redirect*
    /// modifier.
    RedirectLookupLimitExceeded,
    /// The target domain for a *redirect* modifier was invalid.
    InvalidRedirectTargetName,
    /// An *exp* modifier is being evaluated.
    EvaluateExplanation(Explanation),

    // Target name
    /// A target name for a term has been determined.
    TargetName(Name),

    // Lookup limits
    /// The global lookup count is being incremented.
    IncrementLookupCount,
    /// The per-mechanism lookup count is being incremented.
    IncrementPerMechanismLookupCount,
    /// The void lookup count is being incremented.
    IncrementVoidLookupCount,

    // Explanation
    /// The default explanation is being returned.
    DefaultExplanation,
    /// An error occurred while querying for an explain string.
    ExplainStringLookupError(LookupError),
    /// No explain string was found.
    NoExplainString,
    /// Multiple explain strings were found.
    MultipleExplainStrings(Vec<String>),
    /// An explain string could not be parsed.
    InvalidExplainStringSyntax(String),
    /// A custom modification is being applied to an explain string obtained
    /// from the DNS.
    ModifyExplainString(ExplainString),
    /// An explanation string obtained from the DNS is being returned.
    ExplanationStringResult(ExplanationString),
    /// The default explanation is being returned after obtaining an explanation
    /// from the DNS failed.
    ExplanationStringErrorResult(EvalError),

    // Macros
    /// A domain specification is being expanded.
    ExpandDomainSpec(DomainSpec),
    /// An explain string is being expanded.
    ExpandExplainString(ExplainString),

    // Lookups
    /// The DNS is queried for a domain’s A records.
    LookupA(Name),
    /// The DNS is queried for a domain’s AAAA records.
    LookupAaaa(Name),
    /// The DNS is queried for a domain’s MX records.
    LookupMx(Name),
    /// The DNS is queried for a domain’s TXT records.
    LookupTxt(Name),
    /// The DNS is queried for an IP address’s PTR records (reverse lookup).
    LookupPtr(IpAddr),
}

impl Display for Tracepoint {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::ExecuteQuery(name) => write!(f, "executing SPF query for domain \"{name}\""),
            Self::QueryResult(spf_result) => write!(f, "evaluated SPF query to result \"{spf_result}\""),

            Self::SpfRecordLookupError(error) => write!(f, "error querying for SPF record: {error}"),
            Self::NoSpfRecord => write!(f, "no SPF record found"),
            Self::MultipleSpfRecords(records) => {
                write!(f, "multiple SPF records found: ")?;
                fmt_elements(f, records)
            }
            Self::InvalidSpfRecordSyntax(string) => write!(f, "could not parse SPF record \"{string}\""),

            Self::EvaluateSpfRecord(record) => write!(f, "evaluating SPF record \"{record}\""),
            Self::MultipleRedirectModifiers(redirects) => {
                write!(f, "multiple \"redirect\" modifiers found in SPF record: ")?;
                fmt_elements(f, redirects)
            }
            Self::MultipleExpModifiers(explanations) => {
                write!(f, "multiple \"exp\" modifiers found in SPF record: ")?;
                fmt_elements(f, explanations)
            }
            Self::EvaluateDirective(directive) => write!(f, "evaluating directive \"{directive}\""),
            Self::EvaluateMechanism(mechanism) => write!(f, "evaluating mechanism \"{mechanism}\""),
            Self::MechanismMatch => write!(f, "mechanism matched"),
            Self::MechanismNoMatch => write!(f, "mechanism did not match"),
            Self::MechanismErrorResult(error, spf_result) => {
                write!(f, "evaluated mechanism to result \"{spf_result}\" after evaluation failed with error: {error}")
            }
            Self::DirectiveResult(spf_result) => write!(f, "evaluated directive to result \"{spf_result}\""),
            Self::RedirectResult(spf_result) => write!(f, "evaluated redirect modifier to result \"{spf_result}\""),
            Self::NeutralResult => write!(f, "evaluated to default result \"neutral\""),

            Self::TryIpAddr(ip) => write!(f, "trying IP address {ip}"),
            Self::TryMxName(mx) => write!(f, "trying MX name \"{mx}\""),
            Self::ValidatePtrName(ptr) => write!(f, "validating PTR name \"{ptr}\""),
            Self::ReverseLookupError(error) => write!(f, "reverse lookup failed with error: {error}"),
            Self::PtrAddressLookupLimitExceeded => write!(f, "PTR address lookup limit exceeded, ignoring rest"),
            Self::PtrAddressLookupError(error) => write!(f, "lookup of PTR addresses failed with error: {error}"),
            Self::PtrNameValidated => write!(f, "PTR name validated"),
            Self::TryValidatedName(name) => write!(f, "trying validated domain name \"{name}\""),
            Self::TryValidatedNameExactMatch(name) => {
                write!(f, "trying validated domain name (exact match) \"{name}\"")
            }
            Self::TryValidatedNameSubdomainMatch(name) => {
                write!(f, "trying validated domain name (subdomain match) \"{name}\"")
            }
            Self::DefaultUnknownValidatedName => write!(f, "no validated name, use default"),
            Self::EvaluateRedirect(redirect) => write!(f, "evaluating redirect \"{redirect}\""),
            Self::RedirectNoSpfRecord => write!(f, "no SPF record found at redirect domain"),
            Self::RedirectLookupLimitExceeded => write!(f, "global lookup limit exceeded while evaluating redirect"),
            Self::InvalidRedirectTargetName => write!(f, "failed to determine valid target name for redirect"),
            Self::EvaluateExplanation(explanation) => write!(f, "evaluating explanation \"{explanation}\""),

            Self::TargetName(name) => write!(f, "using target name \"{name}\""),

            Self::IncrementLookupCount => write!(f, "incrementing global lookup count"),
            Self::IncrementPerMechanismLookupCount => write!(f, "incrementing per-mechanism lookup count"),
            Self::IncrementVoidLookupCount => write!(f, "incrementing void lookup count"),

            Self::DefaultExplanation => write!(f, "using default explanation string"),
            Self::ExplainStringLookupError(error) => write!(f, "error querying for explain string: {error}"),
            Self::NoExplainString => write!(f, "no explain string found"),
            Self::MultipleExplainStrings(records) => {
                write!(f, "multiple explain strings found: ")?;
                fmt_elements(f, records)
            }
            Self::InvalidExplainStringSyntax(string) => write!(f, "could not parse explain string \"{string}\""),
            Self::ModifyExplainString(explain_string) => {
                write!(f, "applying custom modification to explain string \"{explain_string}\"")
            }
            Self::ExplanationStringResult(s) => write!(f, "obtained explanation string \"{s}\" from DNS"),
            Self::ExplanationStringErrorResult(error) => {
                write!(f, "substituting default explanation string after failure to obtain explanation from DNS with error: {error}")
            }

            Self::ExpandDomainSpec(domain_spec) => write!(f, "expanding domain specification \"{domain_spec}\""),
            Self::ExpandExplainString(explain_string) => write!(f, "expanding explain string \"{explain_string}\""),

            Self::LookupA(name) => write!(f, "looking up A records for \"{name}\""),
            Self::LookupAaaa(name) => write!(f, "looking up AAAA records for \"{name}\""),
            Self::LookupMx(name) => write!(f, "looking up MX records for \"{name}\""),
            Self::LookupTxt(name) => write!(f, "looking up TXT records for \"{name}\""),
            Self::LookupPtr(ip) => write!(f, "looking up PTR records for {ip}"),
        }
    }
}

fn fmt_elements<I, T>(f: &mut Formatter<'_>, iterator: I) -> fmt::Result
where
    I: IntoIterator<Item = T>,
    T: Display,
{
    iterator
        .into_iter()
        .map(|s| format!("\"{s}\""))
        .collect::<Vec<_>>()
        .join(", ")
        .fmt(f)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add_event_ok() {
        let mut trace = Trace::new();
        trace.add_event(Tracepoint::NoSpfRecord);
        assert_eq!(trace.events.len(), 1);
    }
}
