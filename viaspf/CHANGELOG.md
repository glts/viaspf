# viaspf changelog

All notable, publicly visible changes are listed in this changelog.

This project follows Rust-flavoured [semantic versioning], with changes to the
minimum supported Rust version being considered breaking changes.

[semantic versioning]: https://doc.rust-lang.org/cargo/reference/semver.html

## 0.7.0-alpha.2 (2024-05-06)

With this release, development has moved from GitLab to the [Codeberg] platform.

The minimum supported Rust version is now 1.71.0.

[Codeberg]: https://codeberg.org

### Changed

* The minimum supported Rust version has been raised to 1.71.0.
* The dependency trust-dns-resolver has been replaced with the rebranded
  hickory-resolver 0.24.0. Consequently, Cargo feature `trust-dns-resolver` is
  now named `hickory-resolver`.

## 0.6.0 (2023-09-12)

The minimum supported Rust version is now 1.65.0.

### Changed

* The minimum supported Rust version has been raised to 1.65.0.

* The dependency viaspf-record has been upgraded to version 0.5.0. This includes
  the notable renaming of the struct `Record` to `SpfRecord`.

  In accordance with this change, the tracepoint `Tracepoint::EvaluateRecord`
  has been renamed to `Tracepoint::EvaluateSpfRecord`, and the error variant
  `EvalError::IncludeNoRecord` has been renamed to
  `EvalError::IncludeNoSpfRecord`.

* The dependency trust-dns-resolver has been upgraded to version 0.23.0.

## 0.5.3 (2023-03-31)

### Fixed

* Revert the unintended MSRV bump to 1.63.0 in the previous release. The minimum
  supported Rust version remains version 1.61.0.

## 0.5.2 (2023-03-02)

### Fixed

* IPv4-mapped IPv6 address arguments are now properly converted to and therefore
  treated like IPv4 addresses. Contributed by Romain Gérard
  ([glts/viaspf#3](https://gitlab.com/glts/viaspf/-/merge_requests/3)).

## 0.5.1 (2023-02-15)

### Added

* The dependency crate `viaspf_record` is now re-exported in the viaspf API from
  module `viaspf::record`.

  Packages depending on both viaspf and viaspf-record can – and should – now
  drop the latter and access its data types through module `viaspf::record`.
  (If, in a future release, viaspf-record is integrated entirely into viaspf, it
  will only be accessible in this manner.)

## 0.5.0 (2022-11-16)

The minimum supported Rust version is now 1.61.0.

### Changed

* The minimum supported Rust version has been raised to 1.61.0.
* The dependency [trust-dns-resolver] has been upgraded to version 0.22.0.
* Some small improvements to the wording of error `Display` representations.

### Removed

* The unused tracepoint `Tracepoint::PtrNameLookupError` has been removed.

## 0.4.1 (2022-05-30)

### Changed

* The DNS lookup limits implementation has been revised
  ([glts/viaspf#3](https://codeberg.org/glts/viaspf/issues/3)). Treatment of the
  global lookup limit and the per-mechanism lookup limit for the *mx* and *ptr*
  mechanisms and the *p* macro now more accurately follows the intended
  interpretation of the RFC text.

  The result is much more lenient behaviour. You will find that *permerror*
  results due to exceeded lookup limits occur less frequently.

* The previously private struct field [`Trace::events`] is now public.

### Added

* The trait `Lookup` is now implemented for all `Box<T>` where `T: Lookup +
  ?Sized`.
* The new tracepoints `Tracepoint::QueryResult` and
  `Tracepoint::PtrAddressLookupError` have been added.

[`Trace::events`]: https://docs.rs/viaspf/0.4.1/viaspf/trace/struct.Trace.html#structfield.events

## 0.4.0 (2022-03-06)

**This is a backwards-incompatible release.** The API has undergone a complete
overhaul.

Most importantly, the library is now asynchronous. API functions are `async` and
need to be `await`ed. If you want to continue using viaspf in a synchronous
context, you must yourself block on the future returned from these functions
using an async runtime.

Also of note, inputs to the API functions are now more strongly typed. Parsing
of the sender identity becomes a responsibility of the caller. See the
documentation for code examples.

The minimum supported Rust version is now 1.56.1.

### Changed

* The minimum supported Rust version is now 1.56.1, using Rust edition 2021.
* The synchronous `evaluate_spf` function has been replaced with the new
  asynchronous [`evaluate_sender`] function
  ([glts/viaspf#2](https://codeberg.org/glts/viaspf/issues/2)). The arguments of
  this function are no longer of type string, instead you must parse the inputs
  into the new argument types [`Sender`] and [`DomainName`] yourself.
* The field `QueryResult::result` has been renamed to `QueryResult::spf_result`.
* The variant `ExplanationString::Dns` has been renamed to
  `ExplanationString::External`.
* The module `viaspf::record` has been extracted into separate crate
  [viaspf-record].
* The `Lookup` trait and supporting types are now located in separate module
  `viaspf::lookup`. `Lookup` now has supertraits `Send` and `Sync`.
* When the feature `trust-dns-resolver` is enabled, the trait `Lookup` is now
  implemented for the Tokio-based asynchronous
  `trust_dns_resolver::TokioAsyncResolver` instead of the synchronous
  `trust_dns_resolver::Resolver`.
* The query timeout (`Config::timeout`) is now a precise timeout, not a
  best-effort approximation. It is only effective when the (default) feature
  `tokio-timeout` is enabled.
* The dependency [trust-dns-resolver] has been upgraded to version 0.21.
* The changelog is now maintained in a more structured format, similar to
  https://keepachangelog.com.

### Removed

* The ‘default explanation’ (`Config::default_explanation`) returned in the
  `ExplanationString::Default` variant when no external explanation was found
  has been removed. Use the new `expand_explain_string` function to create a
  custom explanation.
* `Name::domain` has been removed in favour of `DomainName::new`. `Name` is now
  purely a supporting type for `Lookup` implementations.

### Added

* The new [`expand_explain_string`] function allows macro-expanding *explain
  strings* (macro strings).
* The maximum number of DNS lookups per query (10 by default) can be configured
  with `ConfigBuilder::max_lookups`.

### Fixed

* Expanding the `t` macro no longer panics when the current system time is
  before the UNIX epoch.

[`evaluate_sender`]: https://docs.rs/viaspf/0.4.0/viaspf/fn.evaluate_sender.html
[`Sender`]: https://docs.rs/viaspf/0.4.0/viaspf/struct.Sender.html
[`DomainName`]: https://docs.rs/viaspf/0.4.0/viaspf/struct.DomainName.html
[viaspf-record]: https://crates.io/crates/viaspf-record
[`expand_explain_string`]: https://docs.rs/viaspf/0.4.0/viaspf/fn.expand_explain_string.html

## 0.3.2 (2021-10-18)

* Make `trust-dns-resolver` an optional dependency. When the feature
  `trust-dns-resolver` is enabled, the trait `Lookup` is implemented for
  `trust_dns_resolver::Resolver`. Such a `Resolver` can then be passed to
  `evaluate_spf` without further ceremony.

  The `spfquery` example now requires the feature `trust-dns-resolver`.

* Relax argument validation requirements when constructing a `DomainSpec`.
* Update dependencies in `Cargo.lock`.

## 0.3.1 (2021-03-10)

* Fix panic while parsing an invalid SPF record containing a non-ASCII UTF-8
  byte sequence.

## 0.3.0 (2021-01-31)

* Validate a sender address’s *local-part* more accurately in line with RFC 5321
  (instead of RFC 3696).
* Rewrite parsing implementation using standard library only, and drop
  dependency on `nom`.
* Update dev dependency `trust-dns-resolver` to version 0.20.
* Update dependencies in `Cargo.lock`.
* Increase minimum supported Rust version to 1.45.0.

## 0.2.1 (2020-12-12)

* Improve parsing of *domain-spec* tokens. Previously, an SPF record containing
  a mechanism that specified a *domain-spec* ending with a *macro-expand* token,
  followed by a CIDR prefix length (eg, `a:mail.%{d}/24`) could not be parsed.

## 0.2.0 (2020-11-03)

* (breaking change) Require that the closure passed to
  `ConfigBuilder::modify_exp_with` is `Send + Sync`. This allows `ConfigBuilder`
  and `Config` to become `Send` and `Sync`
  ([glts/viaspf#1](https://codeberg.org/glts/viaspf/issues/1)).
* Loosen version requirement for dev dependency `trust-dns-resolver` from an
  exact version (`=`) to the default caret version (`^`). This dependency is
  still in alpha and it makes little sense to pin a moving target.

## 0.1.0 (2020-10-22)

Initial release.

[trust-dns-resolver]: https://crates.io/crates/trust-dns-resolver
