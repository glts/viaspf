use hickory_resolver::TokioAsyncResolver;
use std::{env, process};
use viaspf::{Config, Sender, SpfResultCause};

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let mut args = env::args();

    let (ip, sender) = match (
        args.next().as_deref(),
        args.next().and_then(|ip| ip.parse().ok()),
        args.next().and_then(|domain| Sender::from_domain(&domain).ok()),
    ) {
        (_, Some(ip), Some(sender)) => (ip, sender),
        (program, ..) => {
            eprintln!("usage: {} <ip> <domain>", program.unwrap_or("spfquery"));
            process::exit(1);
        }
    };

    let resolver = TokioAsyncResolver::tokio(Default::default(), Default::default());
    let config = Config::builder().capture_trace(true).build();

    let result = viaspf::evaluate_sender(&resolver, &config, ip, &sender, None).await;

    println!("IP: {ip}");
    println!("Domain: {}", sender.domain());
    println!("SPF result: {}", result.spf_result);

    if let Some(cause) = result.cause {
        match cause {
            SpfResultCause::Match(mechanism) => println!("Mechanism: {mechanism}"),
            SpfResultCause::Error(error) => println!("Problem: {error}"),
        }
    }

    if let Some(trace) = result.trace {
        println!("Trace:");
        for event in trace {
            println!("  {}", event.tracepoint);
        }
    }
}
