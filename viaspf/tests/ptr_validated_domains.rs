mod common;

pub use common::*;
use std::net::{IpAddr, Ipv4Addr};
use viaspf::{lookup::*, *};

#[tokio::test]
async fn ptr_with_matching_validated_domain() {
    let lookup = MockLookup::new()
        .with_lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 ptr -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_a(|name| match name.as_str() {
            "mx1.example.com." => Ok(vec![Ipv4Addr::new(34, 56, 78, 90)]),
            "mx2.example.com." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_ptr(|ip| match ip {
            IpAddr::V4(addr) if addr.octets() == [12, 34, 56, 78] => Ok(vec![
                Name::new("mx1.example.com").unwrap(),
                Name::new("mx2.example.com").unwrap(),
            ]),
            _ => Err(LookupError::NoRecords),
        });

    let result = evaluate_sender(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(result.spf_result, SpfResult::Pass);
}

#[tokio::test]
async fn ptr_with_foreign_domain_fails() {
    let lookup = MockLookup::new()
        .with_lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 ptr -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_a(|name| match name.as_str() {
            "other.domain.com." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_ptr(|ip| match ip {
            IpAddr::V4(addr) if addr.octets() == [12, 34, 56, 78] => {
                Ok(vec![Name::new("other.domain.com").unwrap()])
            }
            _ => Err(LookupError::NoRecords),
        });

    let result = evaluate_sender(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(result.spf_result, SpfResult::Fail(Default::default()));
}

#[tokio::test]
async fn ptr_with_validated_domain_in_domain_spec() {
    let lookup = MockLookup::new()
        .with_lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 ptr:domain.com -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_a(|name| match name.as_str() {
            "other.domain.com." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_ptr(|ip| match ip {
            IpAddr::V4(addr) if addr.octets() == [12, 34, 56, 78] => {
                Ok(vec![Name::new("other.domain.com").unwrap()])
            }
            _ => Err(LookupError::NoRecords),
        });

    let result = evaluate_sender(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(result.spf_result, SpfResult::Pass);
}

#[tokio::test]
async fn validated_domain_macro() {
    let lookup = MockLookup::new()
        .with_lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 exists:%{p}._ptr.example.com -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_a(|name| match name.as_str() {
            "other.domain.com." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
            "x1.example.com." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
            "x1.example.com._ptr.example.com." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_ptr(|ip| match ip {
            IpAddr::V4(addr) if addr.octets() == [12, 34, 56, 78] => Ok(vec![
                Name::new("other.domain.com").unwrap(),
                Name::new("x1.example.com").unwrap(),
            ]),
            _ => Err(LookupError::NoRecords),
        });

    let result = evaluate_sender(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(result.spf_result, SpfResult::Pass);
}
