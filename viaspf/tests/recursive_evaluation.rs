mod common;

pub use common::*;
use std::net::IpAddr;
use viaspf::{lookup::*, *};

#[tokio::test]
async fn included_and_redirected_query() {
    let lookup = MockLookup::new()
        .with_lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 ~include:x1.example.com -all".into()]),
            "x1.example.com." => Ok(vec!["v=spf1 redirect=x2.example.com".into()]),
            "x2.example.com." => Ok(vec!["v=spf1 redirect=x3.example.com".into()]),
            "x3.example.com." => Ok(vec!["v=spf1 +all".into()]),
            _ => Err(LookupError::NoRecords),
        });

    let result = evaluate_sender(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(result.spf_result, SpfResult::Softfail);
}

#[tokio::test]
async fn redirected_included_query_with_explanation() {
    let lookup = MockLookup::new()
        .with_lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec![
                "v=spf1 redirect=x1.example.com exp=x0._exp.example.com".into(),
            ]),
            "x1.example.com." => Ok(vec![
                "v=spf1 -include:x2.example.com exp=x1._exp.example.com".into(),
            ]),
            "x2.example.com." => Ok(vec!["v=spf1 +all exp=x2._exp.example.com".into()]),
            "x0._exp.example.com." => panic!(),
            "x1._exp.example.com." => Ok(vec!["explanation%_x1".into()]),
            "x2._exp.example.com." => panic!(),
            _ => Err(LookupError::NoRecords),
        });

    let result = evaluate_sender(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(
        result.spf_result,
        SpfResult::Fail(ExplanationString::External("explanation x1".into()))
    );
}

#[tokio::test]
async fn included_spf_record_missing() {
    let lookup = MockLookup::new()
        .with_lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 include:none.example.com -all".into()]),
            _ => Err(LookupError::NoRecords),
        });

    let result = evaluate_sender(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(result.spf_result, SpfResult::Permerror);
    assert_eq!(
        result.cause,
        Some(SpfResultCause::Error(ErrorCause::NoSpfRecord))
    );
}
