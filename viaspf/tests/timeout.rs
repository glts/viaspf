#![cfg(feature = "tokio-timeout")]

use async_trait::async_trait;
use std::{
    future::Future,
    net::{IpAddr, Ipv4Addr, Ipv6Addr},
    pin::Pin,
    time::Duration,
};
use tokio::time;
use viaspf::{lookup::*, *};

struct MockLookup {
    lookup_a:
        Box<dyn for<'a> Fn(&'a Name) -> Pin<Box<dyn Future<Output = LookupResult<Vec<Ipv4Addr>>> + Send + 'a>>
            + Send + Sync>,
    lookup_txt:
        Box<dyn for<'a> Fn(&'a Name) -> Pin<Box<dyn Future<Output = LookupResult<Vec<String>>> + Send + 'a>>
            + Send + Sync>,
}

#[async_trait]
impl Lookup for MockLookup {
    async fn lookup_a(&self, name: &Name) -> LookupResult<Vec<Ipv4Addr>> {
        self.lookup_a.as_ref()(name).await
    }

    async fn lookup_aaaa(&self, _: &Name) -> LookupResult<Vec<Ipv6Addr>> {
        unimplemented!();
    }

    async fn lookup_mx(&self, _: &Name) -> LookupResult<Vec<Name>> {
        unimplemented!();
    }

    async fn lookup_txt(&self, name: &Name) -> LookupResult<Vec<String>> {
        self.lookup_txt.as_ref()(name).await
    }

    async fn lookup_ptr(&self, _: IpAddr) -> LookupResult<Vec<Name>> {
        unimplemented!();
    }
}

#[tokio::test]
async fn slow_lookup_times_out() {
    let lookup = MockLookup {
        lookup_txt: Box::new(|name| {
            Box::pin(async move {
                match name.as_str() {
                    "example.com." => Ok(vec!["v=spf1 a -all".into()]),
                    _ => Err(LookupError::NoRecords),
                }
            })
        }),
        lookup_a: Box::new(|name| {
            Box::pin(async move {
                time::sleep(Duration::from_secs(15)).await;

                match name.as_str() {
                    "example.com." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
                    _ => Err(LookupError::NoRecords),
                }
            })
        }),
    };

    time::pause();

    let result = evaluate_sender(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    time::resume();

    assert_eq!(result.spf_result, SpfResult::Pass);

    time::pause();

    let config = Config::builder().timeout(Duration::from_secs(10)).build();
    let result = evaluate_sender(
        &lookup,
        &config,
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    time::resume();

    assert_eq!(result.spf_result, SpfResult::Temperror);
    assert_eq!(
        result.cause,
        Some(SpfResultCause::Error(ErrorCause::Timeout))
    );
}
