mod common;

pub use common::*;
use std::net::{IpAddr, Ipv4Addr};
use viaspf::{lookup::*, *};

#[tokio::test]
async fn mx_per_mech_lookup_limit() {
    let lookup = MockLookup::new()
        .with_lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 mx:a.example.com mx:b.example.com -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_mx(|name| match name.as_str() {
            "a.example.com." => Ok(vec![Name::new("mx1.example.com").unwrap()]),
            "b.example.com." => Ok(vec![
                Name::new("mx1.example.com").unwrap(),
                Name::new("mx2.example.com").unwrap(),
                Name::new("mx3.example.com").unwrap(),
                Name::new("mx4.example.com").unwrap(),
                Name::new("mx5.example.com").unwrap(),
                Name::new("mx6.example.com").unwrap(),
                Name::new("mx7.example.com").unwrap(),
                Name::new("mx8.example.com").unwrap(),
                Name::new("mx9.example.com").unwrap(),
                Name::new("mx10.example.com").unwrap(),
                Name::new("mx11.example.com").unwrap(),
            ]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_a(|name| match name.as_str() {
            "mx1.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 1)]),
            "mx2.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 2)]),
            "mx3.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 3)]),
            "mx4.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 4)]),
            "mx5.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 5)]),
            "mx6.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 6)]),
            "mx7.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 7)]),
            "mx8.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 8)]),
            "mx9.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 9)]),
            "mx10.example.com." => Ok(vec![
                Ipv4Addr::new(1, 1, 1, 10),
                Ipv4Addr::new(12, 34, 56, 78),
            ]),
            "mx11.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 11)]),
            _ => Err(LookupError::NoRecords),
        });

    let config = Default::default();

    let result = evaluate_sender(
        &lookup,
        &config,
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(result.spf_result, SpfResult::Pass);

    let result = evaluate_sender(
        &lookup,
        &config,
        IpAddr::from([1, 1, 1, 11]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(result.spf_result, SpfResult::Permerror);
    assert_eq!(
        result.cause,
        Some(SpfResultCause::Error(ErrorCause::LookupLimitExceeded))
    );
}

#[tokio::test]
async fn ptr_per_mech_lookup_limit() {
    let lookup = MockLookup::new()
        .with_lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 ptr:x.example.com -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_ptr(|ip| match ip {
            IpAddr::V4(addr) if addr.octets() == [12, 34, 56, 78] => Ok(vec![
                Name::new("x1.x.example.com").unwrap(),
                Name::new("x2.x.example.com").unwrap(),
                Name::new("x3.x.example.com").unwrap(),
                Name::new("x4.x.example.com").unwrap(),
            ]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_a(|name| match name.as_str() {
            "x1.x.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 1)]),
            "x2.x.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 2)]),
            "x3.x.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 3)]),
            "x4.x.example.com." => Ok(vec![
                Ipv4Addr::new(1, 1, 1, 4),
                Ipv4Addr::new(12, 34, 56, 78),
            ]),
            _ => Err(LookupError::NoRecords),
        });

    let config = Default::default();

    let result = evaluate_sender(
        &lookup,
        &config,
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(result.spf_result, SpfResult::Pass);

    let config = Config::builder().max_lookups(3).build();

    let result = evaluate_sender(
        &lookup,
        &config,
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(result.spf_result, SpfResult::Fail(Default::default()));
}

#[tokio::test]
async fn custom_void_lookup_limit() {
    let lookup = MockLookup::new()
        .with_lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 a:x1.com a:x2.com a:x3.com a:x4.com -all".into()]),
            _ => Err(LookupError::NoRecords),
        });

    let config = Config::builder().max_void_lookups(3).build();

    let result = evaluate_sender(
        &lookup,
        &config,
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(result.spf_result, SpfResult::Permerror);
    assert_eq!(
        result.cause,
        Some(SpfResultCause::Error(ErrorCause::VoidLookupLimitExceeded))
    );

    let config = Config::builder().max_void_lookups(4).build();

    let result = evaluate_sender(
        &lookup,
        &config,
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(result.spf_result, SpfResult::Fail(Default::default()));
}
