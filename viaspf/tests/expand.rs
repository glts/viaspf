mod common;

pub use common::*;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use viaspf::{lookup::Lookup, record::ExplainString, Config, Sender};

struct Expander<T: Lookup> {
    lookup: T,
    config: Config,
    ip: Ipv4Addr,
    ipv6: Ipv6Addr,
    sender: Sender,
}

impl<T: Lookup> Expander<T> {
    async fn macro_string(&self, s: &str) -> String {
        self.expand_explain_string(s, self.ip.into()).await
    }

    async fn macro_string_ipv6(&self, s: &str) -> String {
        self.expand_explain_string(s, self.ipv6.into()).await
    }

    async fn expand_explain_string(&self, s: &str, addr: IpAddr) -> String {
        let e = s.parse::<ExplainString>().unwrap();

        viaspf::expand_explain_string(&self.lookup, &self.config, &e, addr, &self.sender, None)
            .await
            .expansion
    }
}

// See §7.4.
#[tokio::test]
async fn expansion_examples() {
    let exp = Expander {
        lookup: MockLookup::new(),
        config: Default::default(),
        ip: Ipv4Addr::new(192, 0, 2, 3),
        ipv6: Ipv6Addr::new(0x2001, 0xdb8, 0, 0, 0, 0, 0, 0xcb01),
        sender: "strong-bad@email.example.com".parse().unwrap(),
    };

    assert_eq!(exp.macro_string("%{s}").await, "strong-bad@email.example.com");
    assert_eq!(exp.macro_string("%{o}").await, "email.example.com");
    assert_eq!(exp.macro_string("%{d}").await, "email.example.com");
    assert_eq!(exp.macro_string("%{d4}").await, "email.example.com");
    assert_eq!(exp.macro_string("%{d3}").await, "email.example.com");
    assert_eq!(exp.macro_string("%{d2}").await, "example.com");
    assert_eq!(exp.macro_string("%{d1}").await, "com");
    assert_eq!(exp.macro_string("%{dr}").await, "com.example.email");
    assert_eq!(exp.macro_string("%{d2r}").await, "example.email");
    assert_eq!(exp.macro_string("%{l}").await, "strong-bad");
    assert_eq!(exp.macro_string("%{l-}").await, "strong.bad");
    assert_eq!(exp.macro_string("%{lr}").await, "strong-bad");
    assert_eq!(exp.macro_string("%{lr-}").await, "bad.strong");
    assert_eq!(exp.macro_string("%{l1r-}").await, "strong");

    assert_eq!(
        exp.macro_string("%{ir}.%{v}._spf.%{d2}").await,
        "3.2.0.192.in-addr._spf.example.com"
    );
    assert_eq!(
        exp.macro_string("%{lr-}.lp._spf.%{d2}").await,
        "bad.strong.lp._spf.example.com"
    );
    assert_eq!(
        exp.macro_string("%{lr-}.lp.%{ir}.%{v}._spf.%{d2}").await,
        "bad.strong.lp.3.2.0.192.in-addr._spf.example.com"
    );
    assert_eq!(
        exp.macro_string("%{ir}.%{v}.%{l1r-}.lp._spf.%{d2}").await,
        "3.2.0.192.in-addr.strong.lp._spf.example.com"
    );
    assert_eq!(
        exp.macro_string("%{d2}.trusted-domains.example.net").await,
        "example.com.trusted-domains.example.net"
    );

    assert_eq!(
        exp.macro_string_ipv6("%{ir}.%{v}._spf.%{d2}").await,
        "1.0.b.c.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.8.b.d.0.1.0.0.2.ip6._spf.example.com"
    );
}
