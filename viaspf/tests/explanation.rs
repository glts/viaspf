mod common;

pub use common::*;
use std::net::{IpAddr, Ipv4Addr};
use viaspf::{lookup::*, record::Mechanism, *};

#[tokio::test]
async fn explanation_from_dns() {
    let lookup = MockLookup::new()
        .with_lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 -all exp=%{l}.%{h2}.exp._spf.example.com".into()]),
            "amy.example.com.exp._spf.example.com." => Ok(vec!["Das Spiel ist aus, %{l}!".into()]),
            _ => Err(LookupError::NoRecords),
        });

    let result = evaluate_sender(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        &"Amy@example.com".parse().unwrap(),
        Some(&"mail.example.com".parse().unwrap()),
    )
    .await;

    assert_eq!(
        result.spf_result,
        SpfResult::Fail(ExplanationString::External("Das Spiel ist aus, Amy!".into()))
    );
    assert_eq!(result.cause, Some(SpfResultCause::Match(Mechanism::All)));
}

#[tokio::test]
async fn explanation_with_invalid_syntax() {
    let lookup = MockLookup::new()
        .with_lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 -all exp=exp._spf.example.com".into()]),
            "exp._spf.example.com." => Ok(vec!["failure: %{invalid}".into()]),
            _ => Err(LookupError::NoRecords),
        });

    let result = evaluate_sender(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(result.spf_result, SpfResult::Fail(Default::default()));
}

#[tokio::test]
async fn lookup_count_reset_before_exp_evaluation() {
    let lookup = MockLookup::new()
        .with_lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 \
                a:x1.example.com \
                a:x2.example.com \
                a:x3.example.com \
                a:x4.example.com \
                a:x5.example.com \
                a:x6.example.com \
                a:x7.example.com \
                a:x8.example.com \
                a:x9.example.com \
                a:x10.example.com \
                -all \
                exp=%{p}._exp.example.com"
                .into()]),
            "example.com._exp.example.com." => Ok(vec!["failure".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_a(|name| match name.as_str() {
            "example.com." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
            "x1.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 1)]),
            "x2.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 2)]),
            "x3.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 3)]),
            "x4.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 4)]),
            "x5.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 5)]),
            "x6.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 6)]),
            "x7.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 7)]),
            "x8.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 8)]),
            "x9.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 9)]),
            "x10.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 10)]),
            _ => Err(LookupError::NoRecords),
        })
        .with_lookup_ptr(|ip| match ip {
            IpAddr::V4(addr) if addr.octets() == [12, 34, 56, 78] => {
                Ok(vec![Name::new("example.com").unwrap()])
            }
            _ => Err(LookupError::NoRecords),
        });

    let result = evaluate_sender(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        &"amy@example.com".parse().unwrap(),
        None,
    )
    .await;

    assert_eq!(
        result.spf_result,
        SpfResult::Fail(ExplanationString::External("failure".into()))
    );
}
